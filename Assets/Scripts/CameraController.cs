﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
	public Transform araba;
	public float mesafe = 5f;
	public float yukseklik = 2f;
	public float yon = 10;
	private Transform tr;
	
	public Direction myDirection;
	public enum Direction {follow,win };


	public float angularSpeed;
	private Vector3 initialOffset;
	private Vector3 currentOffset;

	void Start()
	{
		followCamera();
		myDirection = Direction.follow;
		tr = transform;
		
	}
	
	void FixedUpdate()
	{
		
			Vector3 hedefKonum = araba.position + new Vector3(yon, yukseklik, -mesafe);
			tr.position = Vector3.Slerp(tr.position, hedefKonum, 0.05f);

			Quaternion lookOnLook =
            Quaternion.LookRotation(araba.position - transform.position);

			transform.rotation =
			Quaternion.Slerp(transform.rotation, lookOnLook, 10*Time.deltaTime);
		
	
	}
	
	public void followCamera()
	{
		myDirection = Direction.follow;
		tr = transform;
        mesafe = 26;
        yukseklik = 10f;
        yon = -30;
    }
	public void winCamera()
	{
		myDirection = Direction.win;
		tr = transform;
		mesafe = 26;
		yukseklik = 10f;
		yon = 0;
	}

}