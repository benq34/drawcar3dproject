﻿using System.Collections;
using UnityEngine;

public class CarObject : MonoBehaviour
{
    public float thrust;
    public Rigidbody rb;
    public bool ground;
    private float maxSpeed = 30;
    void Start()
    {
        if(rb == null)
            rb = GetComponent<Rigidbody>();

       
    }

    void FixedUpdate()
    {
        if (Gamemanager.instance.isStart)
        {
            
         
            if(ground && rb.velocity.magnitude < maxSpeed)
                rb.AddForce(transform.right * thrust*Time.deltaTime);
            
            if (this.gameObject.transform.position.y < -30)
            {
                rb.isKinematic = true;
                Gamemanager.instance.EndGame();
            }

            
        }
        

    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "ground")
        {
            ground = true;
        }
        
        if (collision.transform.tag == "chest")
        {
            
            Camera.main.GetComponent<CameraController>().winCamera();
            StartCoroutine( EndGameRoutine(2.0f));
        }
    }
    void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "ground")
        {
            ground = false;
        }
    }
    
    void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.tag == "coin")
        {
            collider.gameObject.SetActive(false);
        }
      
    }
    
    private IEnumerator EndGameRoutine(float waitTime)
    {
        
            yield return new WaitForSeconds(waitTime);
            Gamemanager.instance.EndGame();
        
    }
}