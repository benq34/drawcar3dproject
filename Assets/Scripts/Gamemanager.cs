﻿using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using UnityEngine;

public class Gamemanager : MonoBehaviour
{
    public StateMachine MainStateMachine;
    public static Gamemanager instance;
    public bool isStart = false;
    public GameObject carContent;
    public GameObject carBody;

    public GameObject coin;
    public GameObject chest;
    public Road[] roads;

    public GameObject map;

    private void Awake()
    {
        instance = this;
        MainStateMachine = this.transform.GetComponent<StateMachine>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        MainStateMachine.ChangeState((int) States.Game);
        isStart = true;

     
        foreach (Transform child in map.transform) {
            child.gameObject.SetActive(true);
        }
        
        carContent.GetComponent<CarObject>().rb.isKinematic = false;
        carContent.transform.position = Vector3.zero;
        carContent.transform.rotation = Quaternion.Euler(0,0,0);
        
        var currentPosition = Vector3.zero;
        
        List<Vector3> newpoints = new List<Vector3>();
        for (int i = 0; i < roads[roads.Length-1].points.Count; i++)
        {
            currentPosition += roads[roads.Length-1].points[i].offset;
            newpoints.Add(currentPosition);
        }
        Vector3 chestOffset=new Vector3(0,2.5f,0.0f);
        GameObject chest = Instantiate(Gamemanager.instance.chest,roads[roads.Length-1].transform);
        chest.transform.localPosition = newpoints[newpoints.Count-1]+chestOffset;

        
      
        Camera.main.GetComponent<CameraController>().followCamera();
        
        
    }


    public void EndGame()
    {
        
        MainStateMachine.ChangeState((int) States.EndGame);
        isStart = false;
        carContent.transform.position = Vector3.zero;
        carContent.transform.rotation = Quaternion.Euler(0,0,0);
        
        
    }

    public enum States
    {
        MainMenu = 0,
        Game = 1,
        EndGame = 2
    }

    public void Menu()
    {
        MainStateMachine.ChangeState((int) States.MainMenu);
        isStart = false;
    }
}
